# Установка

Вся конфигурация сервера описана с помощью ./docker/docker-compose.yml файла
Сервер собран на базе следующих компонентов:

- php 7.3
- nginx 1.15
- redis 5.0.8, используемый в качестве кэширующего сервер а так же в качестве очереди
- postgres в качестве постоянного хранилища - данные в него записываются после обработки очереди
 
Чтобы запустить проект выполните следующие шаги:
   
- cd ./docker/
- docker-compose run -d 
- для того чтобы выполнить миграции бд:  
  docker-compose exec php ./yii migrate


Далее можно открыть сайт по адресу http://localhost

Исходники сервиса лежат в папке app
проект написан на Yii2 фреймворке 
 
 
Примеры запросов:

- Регистрация нового пользователя: 
        
        curl --location --request POST 'http://localhost/user/register' \
        --header 'Content-Type: application/json' \
        --data-raw '{
        "email":"new@gmail.com",
        "password":"password"
        }'


- Создание новой записи о книге: 

        curl --location --request POST 'http://localhost/book/send' \
        --header 'Content-Type: application/json' \
        --header 'X-Api-Key: LSVHABo2L0msjGi9gUBDzJMk2TOmq1g0TPOZqXt7aE2s1hcup0YFRi9Dmo7DrsXB' \
        --data-raw '{
         "title":"Book 2",
         "category":"Cat 1",
         "startedAt":"2019-02-05",
         "finishedAt":"2019-03-07"
         }'
     
- Авторизация зарегистрированного пользователя: 

        curl --location --request POST 'http://localhost/user/login' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "email":"new@gmail.com",
            "password":"password"
        }'  
        
        

Тестовая конфигурация этих запросов для Postman хранится в конфиг-файле
 
        ./Test.postman_collection.json
       
для того, чтобы запустить обработчик очереди нужно запустить следующую комманду: 
./yii book-queue/listen - запустить воркер в виде демона
./yii book-queue/run - запустить воркер, выполнить таски и выйти.
