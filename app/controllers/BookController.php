<?php


namespace app\controllers;


use app\models\StoreBook;
use yii\filters\auth\HttpHeaderAuth;
use yii\rest\Controller;

/**
 * Class BookController
 * @package app\controllers\api
 */
class BookController extends Controller
{
     /**
     * @return array
     */
    public function behaviors():array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpHeaderAuth::class,
        ];
        return $behaviors;
    }

    /**
     * @return StoreBook
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSend():StoreBook
    {
        $model = new StoreBook();
        $model->userId  = \Yii::$app->user->getId();
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        $response = \Yii::$app->response;
        if ($model->validate() && $model->save()) {
            $response->setStatusCode(201);
        }
        return $model;
    }

}