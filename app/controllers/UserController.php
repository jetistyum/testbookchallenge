<?php


namespace app\controllers;

use Yii;
use app\models\UserLogin;
use app\models\UserRegistration;
use yii\rest\Controller;
use yii\web\ServerErrorHttpException;

class UserController extends Controller
{

    /**
     * limit actions types by request type
     * @return array
     */
    protected function verbs():array
    {
        return [
            'login' => ['POST'],
            'register' => ['POST']
        ];
    }

    /**
     * @return UserLogin
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin():UserLogin
    {
        $model = new UserLogin();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if (!$model->login() && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to login for unknown reason.');
        }
        return $model;
    }

    /**
     * @return UserRegistration
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegister():UserRegistration
    {
        $response = Yii::$app->getResponse();
        $model = new UserRegistration();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response->setStatusCode(201);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }
}