<?php

use yii\db\Migration;

/**
 * Class m200227_224105_initial
 */
class m200227_224105_initial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->unsigned(),
            'email' => $this->string()->notNull()->unique(),
            'password'=>$this->string(64),
            'token'=>$this->string(64)->unique(),
            'created_at'=>$this->dateTime(),
        ]);
        $this->createIndex('users_email', 'users', 'email', true);

        $this->createTable('books', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'category' => $this->string()->notNull(),
            'count'=>$this->integer()->defaultValue(0),
        ]);

        $this->createIndex('books_name_categoey_idx', 'books', ['name', 'category'], true);

        $this->createTable('user_books', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'book_id' => $this->integer()->notNull(),
            'started_at'=>$this->date()->notNull(),
            'finished_at'=>$this->date()->notNull(),
        ]);

        $this->addPrimaryKey('user_books_pk', 'user_books', ['user_id', 'book_id']);
        $this->addForeignKey('user_books_books_id_fk', 'user_books', 'book_id', 'books', 'id',  'CASCADE', 'CASCADE');
        $this->addForeignKey('user_books_user_id_fk', 'user_books', 'user_id', 'users', 'id',  'CASCADE', 'CASCADE');




    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_books');
        $this->dropTable('books');
        $this->dropTable('users');
    }

}
