<?php

use app\components\BookComponent;
use app\components\UserComponent;
use yii\queue\redis\Queue;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'container'=>require ('container.php'),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YdmzrOSfUeUaYu8ds82rAs2LUsTL76Wl',
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
            ]
        ],

        'redis' => [
            'class' => yii\redis\Connection::class,
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
        'customer'=>[
            'class'=> UserComponent::class
        ],

        'book'=>[
            'class'=> BookComponent::class,
        ],

        'bookQueue' => [
            'class' => Queue::class,
            'redis' => 'redis', // Redis connection component or its config
            'channel' => 'books_queue', // Queue channel key
        ],

        'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'user' => [
            'identityClass' => app\models\UserIdentity::class,
            'enableAutoLogin' => true,
            'enableSession'=>false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                  'class' => Swift_SmtpTransport::class,
                  'host' => 'mailhog',
                  'port' => '1025',
              ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'reg'=>[
            'class'=> UserComponent::class
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'POST user/register' => 'user/register',
                'POST user/login' => 'user/login',
                'POST book/send' => 'book/send',
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
