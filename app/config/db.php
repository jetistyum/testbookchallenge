<?php

return [
    'class' => 'yii\db\Connection',
    //'dsn' => 'mysql:host=db;dbname=app',
    'dsn' => 'pgsql:host=db;port=5432;dbname=app',
    'username' => 'app',
    'password' => 'app',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
