<?php


namespace app\commands;
use yii\console\Controller;

class ChatController extends Controller {

    public function actionStart() {
        $app = new App('localhost', 8080);
        $app->route('/chatonline', new MyChat(), ['*']);
        $app->run();
    }

}