<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_books".
 *
 * @property int $user_id
 * @property int $book_id
 * @property string $started_at
 * @property string $finished_at
 *
 * @property Books $book
 * @property Users $user
 */
class UserBooks extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id', 'started_at', 'finished_at'], 'required'],
            [['user_id', 'book_id'], 'default', 'value' => null],
            [['user_id', 'book_id'], 'integer'],
            [['started_at', 'finished_at'],  'date', 'format' => 'php:Y-m-d'],
            [['user_id', 'book_id'], 'unique', 'targetAttribute' => ['user_id', 'book_id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::class, 'targetAttribute' => ['book_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels():array
    {
        return [
            'user_id' => 'User ID',
            'book_id' => 'Book ID',
            'started_at' => 'Started At',
            'finished_at' => 'Finished At',
        ];
    }

    /**
     * Gets query for [[Book]].
     *
     * @return \yii\db\ActiveQuery|BooksQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::class, ['id' => 'book_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserBooksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserBooksQuery(get_called_class());
    }
}
