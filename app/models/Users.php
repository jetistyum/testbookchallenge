<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email
 * @property string|null $password
 * @property string|null $token
 * @property string|null $created_at
 */
class Users extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName():string
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['email'], 'required'],
            [['email'], 'unique'],
            //[['created_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['password'], 'string', 'min' =>3],
            [['token'], 'string', 'max' => 64],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels():array
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'created_at' => 'Created At',
        ];
    }



    public function fields():array
    {
        $f = parent::fields();
        unset($f['password']);
        unset($f['id']);
        return $f;
    }


    public static function find():ActiveQuery
    {
        return Yii::createObject(ActiveQuery::class, [get_called_class()]);
    }



}
