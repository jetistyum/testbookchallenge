<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserBooks]].
 *
 * @see UserBooks
 */
class UserBooksQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserBooks[]|array
     */
    public function all($db = null):?array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserBooks|null
     */
    public function one($db = null):?UserBooks
    {
        return parent::one($db);
    }
}
