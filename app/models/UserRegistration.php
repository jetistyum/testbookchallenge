<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email
 * @property string|null $password
 * @property string|null $token
 */
class UserRegistration extends Model
{

    public $token;
    public $id;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['email'], 'required'],
            [['email'], 'unique', 'targetClass'=>Users::class],
            [['email'], 'string', 'max' => 255],
            [['password'], 'string', 'min' =>3],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels():array
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'created_at' => 'Created At',
        ];
    }


    /**
     * only token will be returned to user
     * @return array
     */
    public function fields():array
    {
       return[
           'token'
       ];

    }

    public function save():bool
    {
        if ($this->validate()){
            return \Yii::$app->customer->register($this);
        }
        return false;
    }


}
