<?php

namespace app\models;

class UserIdentity  implements \yii\web\IdentityInterface
{
    protected $user;

    /**
     * UserIdentity constructor.
     * @param Users $user
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id):?self
    {
        $user =  \Yii::$app->customer->getUserById($id);
        if ($user){
            return new self($user);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null):?self
    {
        $user =  \Yii::$app->customer->getUserByToken($token);
        if ($user){
            return new self($user);
        }
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByUsername($email):?self
    {
        $user = \Yii::$app->customer->getUserByEmail($email);
        if ($user){
            return new self($user);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId():int
    {
        return $this->user->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey():string
    {
        return $this->user->token;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey):bool
    {
        return $this->user->token === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password):bool
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->user->password);
    }


}
