<?php


namespace app\models;



use Yii;
use yii\base\Model;
use yii\queue\JobInterface;


class StoreBook extends Model  implements JobInterface
{

    public $title;
    public $category;
    public $startedAt;
    public $finishedAt;
    public $userId;


    /**
     * @return array the validation rules.
     */
    public function rules():array
    {
        return [
            [['title', 'category', 'startedAt', 'finishedAt'], 'required'],
            [['startedAt', 'finishedAt'], 'date', 'format' => 'php:Y-m-d'],
            [['title', 'category'], 'filter', 'filter'=>'strtolower'],
        ];
    }


    /**
     * @return bool
     */
    public function save():bool
    {
        return Yii::$app->book->enqueue($this);
    }

    /**
     * @return array
     */
    public function fields():array
    {
        $f = parent::fields();
        unset($f['userId']);
        return $f;
    }

    /**
     * @inheritDoc
     */
    public function execute($queue):void
    {
        Yii::$app->book->handleQueuedModel($this);
    }

    /**
     * @return Books|null
     */
    public function getBook():?Books
    {
        return Yii::$app->book->findBook($this);
    }
}