<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $name
 * @property string $category
 * @property int|null $count
 *
 * @property UserBooks[] $userBooks
 * @property Users[] $users
 */
class Books extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName():string
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['name', 'category'], 'required'],
            [['count'], 'default', 'value' => 0],
            [['count'], 'integer'],
            [['name', 'category'], 'string', 'max' => 255],
            [['name', 'category'], 'unique', 'targetAttribute' => ['name', 'category']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels():array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category' => 'Category',
            'count' => 'Count',
        ];
    }

    /**
     * Gets query for [[UserBooks]].
     *
     * @return UserBooksQuery
     */
    public function getUserBooks():UserBooksQuery
    {
        return $this->hasMany(UserBooks::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getUsers():ActiveQuery
    {
        return $this->hasMany(Users::class, ['id' => 'user_id'])->viaTable('user_books', ['book_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return BooksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BooksQuery(get_called_class());
    }
}
