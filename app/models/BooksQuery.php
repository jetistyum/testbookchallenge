<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Books]].
 *
 * @see Books
 */
class BooksQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Books[]|array
     */
    public function all($db = null):?array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Books|null
     */
    public function one($db = null):?Books
    {
        return parent::one($db);
    }
}
