<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email
 * @property string|null $password
 * @property string|null $token
 */
class UserLogin extends Model
{

    public $token;
    public $id;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'exist', 'targetClass'=>Users::class],
            [['password'], 'string', 'min' =>3],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels():array
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'created_at' => 'Created At',
        ];
    }


    /**
     * only token will be returned to user
     * @return array
     */
    public function fields():array
    {
        return[
            'token'
        ];

    }

    /**
     * @return bool
     */
    public function login():bool
    {
        if ($this->validate()) {
            $userIdentity = UserIdentity::findByUsername($this->email);
            if ($userIdentity && $userIdentity->validatePassword($this->password)) {
                $this->token = $userIdentity->getAuthKey();
                return true;
            }
            else{
                $this->addError('email', 'Invalid email or password');
            }
        }
        return false;
    }

}
