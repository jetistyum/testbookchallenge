<?php


namespace app\components;

use app\models\Books;
use app\models\StoreBook;
use app\models\UserBooks;
use Yii;
use yii\base\Component;


class BookComponent extends Component
{

    /**
     * @param StoreBook $model
     * @return string|null id of a job message
     */
    public function enqueue(StoreBook $model):?string
    {
       return \Yii::$app->bookQueue->push($model);
    }


    /**
     * @param StoreBook $model
     */
    public function handleQueuedModel(StoreBook $model):void
    {
        if (\Yii::$app->redis->sismember('user_book:' .$model->userId, $model->category.':'.$model->title)){
            return ;
        }
        $this->saveBook($model);
        $this->saveUserBook($model);

    }

    /**
     * @param StoreBook $model
     */
    private function saveBook(StoreBook $model):void
    {
        $book = Books::findOne(['name'=>$model->title, 'category'=>$model->category]);
        if (!$book){
            $book = new Books();
            $book->category = $model->category;
            $book->name = $model->title;
            $book->count = 0;
            if ($book->save()) {
                \Yii::$app->redis->ZADD('books', 'INCR', 1, $model->category . ':' . $model->title);
            }
        }
    }

    /**
     * @param StoreBook $model
     */
    private function saveUserBook(StoreBook $model):void
    {
        $book = $model->getBook();
        if ($book && !UserBooks::find()->where(['user_id'=>$model->userId, 'book_id'=>$book->id])->exists()){
            $userBook = new UserBooks();
            $userBook->user_id = $model->userId;
            $userBook->book_id = $book->id;
            $userBook->started_at = $model->startedAt;
            $userBook->finished_at = $model->finishedAt;
            $userBook->save();
            Books::updateAllCounters(['count'=>1], ['name'=>$model->title, 'category'=>$model->category]);
            \Yii::$app->redis->sadd('user_book:' .$model->userId, $model->category.':'.$model->title);
        }
    }

    /**
     * @param StoreBook $model
     * @return Books|null
     */
    public function findBook(StoreBook $model):?Books
    {
        return Books::find()->where(['name'=>$model->title, 'category'=>$model->category])->cache(10)->one();
    }

}