<?php


namespace app\components;

use app\models\UserLogin;
use app\models\UserRegistration;
use app\models\Users;
use Yii;
use yii\base\Component;


class UserComponent extends Component
{
    /**
     * @param UserRegistration $formModel
     * @return bool
     * @throws \yii\base\Exception
     */
    public function register(UserRegistration $formModel):bool
    {
        $model = new Users();
        $model->attributes = $formModel->attributes;
        $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
        $model->token = $formModel->token = $this->generateToken();
        if ($model->save()) {
            $this->store($model);
            return true;
        }
        return false;
    }

    /**
     * @param string $password
     * @param Users $user
     * @return bool
     */
    public function validatePassword(string $password, Users $user):bool
    {
        return Yii::$app->getSecurity()->validatePassword($password, $user->password);
    }


    /**
     * @return string
     * @throws \yii\base\Exception
     */
    private function generateToken():string
    {
        return Yii::$app->getSecurity()->generateRandomString(64);
    }

    /**
     * @param UserLogin $model
     * @throws \yii\base\Exception
     */
    public function login(UserLogin $model):void
    {
        $this->invalidate($model->token);
        $model->token = $this->generateToken();
        $this->store($model);
    }

    /**
     * @param $token
     * @return Users|null
     */
    public function getUserByToken($token):?Users
    {
        $userArray =  $this->get($token);
        if ($userArray) {
            $user = new Users();
            $user->id = $userArray['id'];
            $user->setAttributes($userArray);
            return $user;
        }
    }

    /**
     * @param string $email
     * @return Users|null
     */
    public function getUserByEmail(string $email):?Users
    {
        return Users::findOne(['email'=>$email]);
    }


    /**
     * @param Users $user
     */
    public function store(Users $user):void
    {
        $str = json_encode($user->attributes);
        Yii::$app->redis->set('user:hash:' . $user->token, $str, 'EX', 3600*24*30);
    }

    /**
     * @param $hash
     * @return array
     */
    private function get($hash):?array
    {
        $user = Yii::$app->redis->get('user:hash:' . $hash);
        if ($user && !empty($user)){
            return json_decode($user, true);
        }
    }

    private function invalidate($hash):void
    {
        Yii::$app->redis->del('user:hash:' . $hash);
    }
}